# Premiers pas avec Open GL

Ca fait un certain temps que je suis en admiration devant les projets qui arrivent à afficher des objets 3d, et des graphiques propres rafraîchis à grande vitesse.

Une technologie de choix pour ces deux défis semble être OpenGL. Il en existe d'autre (DirectX, Vulkan), mais elles sont soit réservées pour le moment à Windows, soit à priori plus bas niveau, pour permettre aux experts de mieux maitriser le shmilblick.

C'est peut être le moment de rentrer un peu dans ce monde merveilleux ?

L'expérience décrite dans cet article utilise python, car plus rapide à mettre en oeuvre, mais tout est bien sûr faisable en C++.

## Installation

J'ai suivi les informations données dans [modern opengl](https://www.labri.fr/perso/nrougier/python-opengl/#modern-opengl).

En résumé, pour faire tourner une petite application OpenGL, il vous faut un *backend*, qui vous fournira la fenêtre et les entrées/sorties clavier et souris. Ce *backend* vous fournira un contexte (ne me demandez pas quelles informations sont contenues dans ce contexte), que vous passerez à opengl afin qu'il puisse afficher ce que vous lui demandez dans la fenêtre.

La plupart des librairies d'IHM proposent de fournir un contexte OpenGL. Les plus basiques sont les plus populaires dans les tutoriels, mais Qt et Gtk par exemple proposent également la fonctionnalité.

Personelement j'utilise la librairie SDL2 comme backend, principalement parce que je la connaissais un peu et qu'elle a fonctionné correctement du premier coup.

## Démarrer

[modern opengl](https://www.labri.fr/perso/nrougier/python-opengl/#modern-opengl) fourni tout ce qu'il faut pour comprendre les bases. C'est la documentation la plus claire que j'aie trouvé pour le moment, en partant de zéro.

## Quand vous en aurez marre des cubes

On peut relativement facilement remplacer le cube par un STL. C'est là que les choses deviennent un peu plus intéressantes.

### Créer un fichier stl

On pourra utiliser [Freecad](https://www.freecadweb.org/) ou [openscad](https://www.openscad.org/) pour les libristes.
Les autres pourront utiliser leur outil de CAO préféré (j'aime bien [Onshape](https://www.onshape.com/en/)).

Pour ceux qui préfèrent une solution rapide en ligne (formes simples):

- [slicecrafter](https://icesl.loria.fr/slicecrafter/)
- [OpenJSCAD](https://openjscad.org/)

Pour ceux qui n'ont pas envie de se compliquer la vie : [Thingiverse](https://www.thingiverse.com/) est une base de donnée d'objets STL gratuits.

### Lire un fichier stl

Le format STL est assez simple, et décrit sur [wikipédia](https://en.wikipedia.org/wiki/STL_(file_format)).
Dans mon cas, le stl dont je disposais était au format binaire.
J'en profite pour rappeler l'existence de la fonction [struct.unpack](https://docs.python.org/3/library/struct.html), qui converti des données binaires en types de base python.

La fonction de lecture du fichier stl dans ce repository est `binary_stl_loader.py`.

### Afficher le fichier stl

Une fois le fichier lu, il suffit de passer les sommets à opengl pour l'afficher, comme on fait dans les tutoriels pour le cube.

*On a alors plusieurs fois chaque sommet*, ce qui ne semble pas géner outre mesure le driver.

### Eclairer l'objet

#### Un peu de théorie

OpenGL ne fait pas (en tous cas à mon niveau) grand chose à notre place. Il propose simplement de parralléliser les calculs pour que chaque pixel aie une valeur calculée le plsu rapidement possible, mais *c'est à nous d"écrire le code correspondant au modèle qu'on veut représenter*.


Dans le cas de l'éclairage, on peut donc s'intéresser à la [photométrie](https://fr.wikipedia.org/wiki/Photom%C3%A9trie_(optique)) et à ses articles connexes, pour se donner une idée de la complexité des modèles qu'on peut créer.


Comme on veut juste faire un exemple rapide, on va prendre un modèle très simple décrit [ici](https://learnopengl.com/Lighting/Basic-Lighting) et basique en 3D.


#### Calculer les normales

Pour éclairer l'objet la première étape consiste à faire le produit scalaire entre un vecteur de direction de la lumière (normalisé) par la normale à chaque sommet (normalisée également) (= la normale à chaque face dans notre cas). On obtient alors une intensité entre 0 et 1 à appliquer à la couleur rendue sur l'objet.

Pour chaque sommet, il faut donc calculer la normale de sa face. Pour cela on fait le produit vectoriel de deux arrètes consécutives, qu'on normalise.

*nb*: On peut alors se retrouver avec plusieurs normales pour un seul sommet si plusieurs arrêtes s'y rejoignent. Il semble que certains fassent la moyenne. Dans le cas du stl, chaque sommet apparait autant de fois qu'il y a de faces dans mon buffer, donc on a bien une normale pour un sommet.

*nb2*: Si lampe et objet ont deux couleurs différentes, il faut définir la combinaison des deux couleurs. On n'en parlera pas ici.

#### Ajouter un modèle d'éclairage au vertex shader


## Ajouter quelques contrôles pour le fun

Pour aller avec ce type de projet, il semble que les librairies du type Dear Imgui soient bien adapté. En effet, leur paradygme consiste à tout recalculer à chaque frame, comme en OpenGL : On ne gère pas d'évènements, on regarde dans quel état est chaque élément à chaque instant. Notre gui n'est qu'une suite d'états.
Il s'agit d'autre part d'une librairie populaire (on la retrouve notamment [créditée par Qt](https://doc.qt.io/qt-5/qt3d-attribution-imgui.html))

voir le script `display_stl_imgui.py`

## Pourquoi se casser la tête quand il existe des frameworks puissants ?

Hem, parce qu'on adore ça ? Parce que cela permet de mieux comprendre les problèmes ?

- Moteurs de jeu : Ogre3d, Unity, Unreal Engine
- Générique : http://www.open3d.org/
- Qt3D
- Web / javascript (probablement du WebGL donc, mais je ne connais pas la différence) BabylonJS

## Biblio

- [nrougier, modern opengl](https://www.labri.fr/perso/nrougier/python-opengl/#modern-opengl). Très bon livre, malheureusement non terminé. Plutôt à destination d'un public souhaitant faire de la visualisation scientifique que 3D. Mon point de départ.
- [learnopengl.com](https://learnopengl.com/Lighting/Basic-Lighting). Tuto et ressources sur OpenGL, pas trop mal.
- [opengl-tutorial.org/](http://www.opengl-tutorial.org/beginners-tutorials/tutorial-8-basic-shading/) Un peu dur pour démarrer à mon goût, mais intéressant une fois qu'on a compris les principes de base.

Une liste plus complète est dispo [sur le wiki officiel](https://www.khronos.org/opengl/wiki/Getting_Started#Tutorials_and_How_To_Guides).

## TODO

- requirements.txt
- Afficher les axes
- Mettre des sliders pour la rotation ?
