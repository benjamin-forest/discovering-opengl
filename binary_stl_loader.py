"""
loads a binary stl file, as described by wikipédia, of the form :

UINT8[80] – Header
UINT32 – Number of triangles

foreach triangle
    REAL32[3] – Normal vector
    REAL32[3] – Vertex 1
    REAL32[3] – Vertex 2
    REAL32[3] – Vertex 3
    UINT16    – Attribute byte count (n)
    UINT8[n]  - Extra attribute data
end

and translate it as ascii format. I verified with Cura that it is correctly understood.

ascii format :

solid name

where name is an optional string (though if name is omitted there must still be a space after solid). The file continues with any number of triangles, each represented as follows:

facet normal ni nj nk
    outer loop
        vertex v1x v1y v1z
        vertex v2x v2y v2z
        vertex v3x v3y v3z
    endloop
endfacet

where each n or v is a floating-point number in sign-mantissa-"e"-sign-exponent format, e.g., "2.648000e-002". The file concludes with

endsolid name

"""
import numpy as np
import struct

HEADER_SIZE = 80

def facets_count(file_name:str):
    """ returns face count in binary stl """
    with open(file_name, 'rb') as stl_file:
            header = struct.unpack("80s", stl_file.read(HEADER_SIZE))
            triangles_count = struct.unpack("I", stl_file.read(4))[0]
            return triangles_count

def facets(file_name:str):
    """
    reads a binary stl and yield for each facet
    """
    with open(file_name, 'rb') as stl_file:
        header = struct.unpack("80s", stl_file.read(HEADER_SIZE))
        triangles_count = struct.unpack("I", stl_file.read(4))[0]
        for i in range(triangles_count):
            vertex=[]
            normal_vec = struct.unpack("3f", stl_file.read(3*4))
            vertex.append( struct.unpack("3f", stl_file.read(3*4)) )
            vertex.append( struct.unpack("3f", stl_file.read(3*4)) )
            vertex.append( struct.unpack("3f", stl_file.read(3*4)) )
            if struct.unpack("H", stl_file.read(2))[0]:
                raise Exception("Found one non null machintruc")
            yield normal_vec, vertex

def load_stl(filename: str, face_color: list):
    """
    loads a binary stl from filename and returns it as a np matrix, ready to be passed to opengl. Color of the object is defined as a 4 elements list,
       :returns: tuple (vertices, indices)
        WHERE
        numpy.ndarray vertices is all the information linked to stl vertices
        numpy.ndarray indices is
    """

    if(len(face_color) != 4):
        raise Exception("face_color shall be a 4 elements list")

    vertices = np.zeros(facets_count(filename)*3, [ ("a_position", np.float32, 3),
                            ("a_color", np.float32, 4), ("a_normal", np.float32, 3)])
    indices = []
    face_normals = []
    i = 0
    for normal, vertex in facets(filename):
        vertices["a_position"][i] = [vertex[0][0], vertex[0][1], vertex[0][2]]
        vertices["a_position"][i+1] = [vertex[1][0], vertex[1][1], vertex[1][2]]
        vertices["a_position"][i+2] = [vertex[2][0], vertex[2][1], vertex[2][2]]

        # Note : here one compute normals from vertices, but the information is also, in my example file, contained in the normal parameter from the loop. Not sure it will be correct for all stls, though.
        edge1 = vertices["a_position"][i+1] - vertices["a_position"][i]
        edge2 = vertices["a_position"][i+2] - vertices["a_position"][i]
        face_normal = np.cross(edge1, edge2)
        norm = np.linalg.norm(face_normal)
        face_normal = face_normal / norm
        vertices["a_normal"][i] = face_normal
        vertices["a_normal"][i+1] = face_normal
        vertices["a_normal"][i+2] = face_normal

        vertices["a_color"][i] = face_color
        vertices["a_color"][i+1] = face_color
        vertices["a_color"][i+2] = face_color

        indices.append(i)
        indices.append(i+1)
        indices.append(i+2)
        i = i +3

        f_indices = np.array(indices, dtype=np.uint32) # Faces : each chunk of three define indexes of a triangle to be rendered.
    return (vertices, f_indices)

def convert_bin_to_ascii(filename:str):
    """ Converts a binary stl to an ascii one """
    with open("ascii_"+filename, "w", encoding='ascii') as ascii_stl_file:
        ascii_stl_file.write(f"solid {filename}\n")
        for normal_vec, vertex in facets(filename) :
            ascii_stl_file.write( f"""facet normal {normal_vec[0]:e} {normal_vec[1]:e} {normal_vec[2]:e}
            outer loop
                vertex {vertex[0][0]:e} {vertex[0][1]:e} {vertex[0][2]:e}
                vertex {vertex[1][0]:e} {vertex[1][1]:e} {vertex[1][2]:e}
                vertex {vertex[2][0]:e} {vertex[2][1]:e} {vertex[2][2]:e}
            endloop
        endfacet\n""" )
        ascii_stl_file.write(f"endsolid {filename}")

if __name__ == "__main__":
    convert_bin_to_ascii(STL_FILE)