"""Try to display a simple stl using python and opengl
Based on book : https://www.labri.fr/perso/nrougier/python-opengl/#modern-opengl
"""

import binary_stl_loader
import opengl_context_creator
import opengl_shaders
import opengl_helpers
import opengl_matrix_helper

import sys
import ctypes

import sdl2

import numpy as np

def run():

    (window, context) = opengl_context_creator.create_sdl2_windowed_gl_context("Tuto !")

    program = opengl_shaders.init_program()

    (vertices, f_indices) = binary_stl_loader.load_stl("openjscad.stl", [1.0,1.0,1.0,1.0])

    gpu = opengl_helpers.share_stl_with_gpu(program, vertices, f_indices)

    opengl_helpers.reshape(512,512, gpu)
    event = sdl2.SDL_Event()
    running = True
    theta, phi = 30, 40
    while running:
        while sdl2.SDL_PollEvent(ctypes.byref(event)) != 0:
            if event.type == sdl2.SDL_QUIT:
                running = False
        theta += 0.5 # degrees
        phi   += 0.5 # degrees
        view_parameters = {"color":(1., .0, .5, 1.), "model": np.eye(4)}
        opengl_matrix_helper.rotate(view_parameters["model"], theta, 0, 0, 1)
        opengl_matrix_helper.rotate(view_parameters["model"], phi, 0, 1, 0)
        opengl_helpers.update_view(window, gpu, f_indices, view_parameters)
        # mandatory, see sdl2 doc
        sdl2.SDL_GL_SwapWindow(window)

    sdl2.SDL_GL_DeleteContext(context)
    sdl2.SDL_DestroyWindow(window)
    sdl2.SDL_Quit()
    return 0

if __name__ == "__main__":
    sys.exit(run())