"""Try to display a simple stl using python and opengl
Based on book : https://www.labri.fr/perso/nrougier/python-opengl/#modern-opengl
"""

import binary_stl_loader
import opengl_context_creator
import opengl_shaders
import opengl_helpers
import opengl_matrix_helper

import sys
import ctypes

import sdl2

import imgui
from imgui.integrations.sdl2 import SDL2Renderer

import numpy as np

def run():

    view_parameters = {"color":(1., .0, .5, 1.), "model": np.eye(4)}

    (window, context) = opengl_context_creator.create_sdl2_windowed_gl_context("Tuto !")

    imgui.create_context()
    impl = SDL2Renderer(window)

    program = opengl_shaders.init_program()

    (vertices, f_indices) = binary_stl_loader.load_stl("openjscad.stl", [1.0,1.0,1.0,1.0])

    gpu = opengl_helpers.share_stl_with_gpu(program, vertices, f_indices)

    opengl_helpers.reshape(512,512, gpu)
    event = sdl2.SDL_Event()
    running = True

    x = ctypes.c_int(0)
    y = ctypes.c_int(0)
    theta = 0
    phi = 0
    while running:
        while sdl2.SDL_PollEvent(ctypes.byref(event)) != 0:
            if event.type == sdl2.SDL_QUIT:
                running = False
            if imgui.get_io().want_capture_mouse:
                impl.process_event(event)
            else:
                if event.type == sdl2.SDL_MOUSEMOTION:
                    state = sdl2.SDL_GetMouseState(ctypes.byref(x), ctypes.byref(y))
                    motion = event.motion
                    if state & sdl2.SDL_BUTTON(sdl2.SDL_BUTTON_LEFT):
                        view_parameters["model"] = opengl_matrix_helper.translate(view_parameters["model"], motion.xrel/10.0, -motion.yrel/10.0, 0)
                    if state & sdl2.SDL_BUTTON(sdl2.SDL_BUTTON_RIGHT):
                        theta = motion.yrel
                        phi = motion.xrel
                        thetar = np.radians(theta)
                        phir = np.radians(phi)
                        c, s = np.cos(thetar), np.sin(thetar)
                        c2, s2 = np.cos(phir), np.sin(phir)
                        Rz = np.array(((c, -s, 0), (s, c, 0), (0,0,1)))
                        Ry = np.array(((c2, 0, s2), (0, 1, 0), (-s2,0,c2)))
                        Rzy = np.matmul(Rz, Ry)
                        Rzy = Rzy.transpose()
                        k_arr = np.matmul(Rzy, np.array((0,0,1)))
                        j_arr = np.matmul(Rzy, np.array((0,1,0)))
                        opengl_matrix_helper.rotate(view_parameters["model"], theta, k_arr[0], k_arr[1], k_arr[2])
                        opengl_matrix_helper.rotate(view_parameters["model"], phi, j_arr[0], j_arr[1], j_arr[2])
                    # if state & sdl2.SDL_BUTTON(sdl2.SDL_BUTTON_MIDDLE):
                    #     view_parameters["model_distance"] +=
        impl.process_inputs()

        imgui.new_frame()

        imgui.begin("Custom window", True)

        # note: first element of return two-tuple notifies if the color was changed
        #       in currently processed frame and second element is current value
        #       of color and alpha
        _, view_parameters["color"] = imgui.color_edit4("Alpha", *view_parameters["color"], show_alpha=True)

        imgui.end()

        opengl_helpers.update_view(window, gpu, f_indices, view_parameters)

        imgui.render()
        impl.render(imgui.get_draw_data())

        # mandatory, see sdl2 doc
        sdl2.SDL_GL_SwapWindow(window)

    impl.shutdown()

    sdl2.SDL_GL_DeleteContext(context)
    sdl2.SDL_DestroyWindow(window)
    sdl2.SDL_Quit()
    return 0

if __name__ == "__main__":
    sys.exit(run())