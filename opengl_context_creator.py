
""" A Simple helper creating a windows + an OpenGL context using sdl library """

from sdl2 import *

def create_sdl2_windowed_gl_context(window_title:str="Default"):
    """ Creates an opengl context inside an sdl2 window.
    Return the context so OpenGL can use it.
    @return (windows, context)
    """
    width, height = 512, 512
    window_name = window_title

    if SDL_Init(SDL_INIT_EVERYTHING) < 0:
        raise Exception("Error: SDL could not initialize! SDL Error: " + SDL_GetError())

    window = SDL_CreateWindow(window_name.encode('utf-8'),
                              SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                              width, height,
                              SDL_WINDOW_OPENGL|SDL_WINDOW_RESIZABLE)

    if window is None:
        raise Exception("Error: Window could not be created! SDL Error: " + SDL_GetError())

    gl_context = SDL_GL_CreateContext(window)
    if gl_context is None:
        raise Exception("Error: Cannot create OpenGL Context! SDL Error: " + str(SDL_GetError()))

    SDL_GL_MakeCurrent(window, gl_context)
    if SDL_GL_SetSwapInterval(1) < 0:
        raise Exception("Warning: Unable to set VSync! SDL Error: " + SDL_GetError())

    return window, gl_context