from OpenGL import GL as gl

import numpy as np

import ctypes

import opengl_matrix_helper

def reshape(width, height, gpu):
    gl.glViewport(0, 0, width, height)
    projection = opengl_matrix_helper.perspective(45.0, width / float(height), 2.0, 500.0)
    gl.glUniformMatrix4fv(gpu["uniform"]["u_projection"], 1, False, projection)

def update_view(window, gpu, f_indices, view_parameters={}):
    """
    Updates the view contained in window.
    Workflow (I think)
    - Clear the existing buffer with a background color
    - Set data in GPU-CPU shared memory
    - Draw everything (using previously defined shaders)
    - Ask window to display newly created buffer
    """

    # Clear previously drawed image
    gl.glDepthMask(gl.GL_TRUE)
    gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)

    ## Update shared memory and draw frame
    gl.glDisable(gl.GL_BLEND)
    gl.glEnable(gl.GL_DEPTH_TEST)
    gl.glEnable(gl.GL_POLYGON_OFFSET_FILL)
    gl.glUniform4f(gpu["uniform"]["u_color"], *view_parameters["color"])
    gl.glUniformMatrix4fv(gpu["uniform"]["u_model"], 1, False, view_parameters["model"])
    gl.glBindBuffer(gl.GL_ELEMENT_ARRAY_BUFFER, gpu["buffer"]["filled"])
    gl.glDrawElements(gl.GL_TRIANGLES, len(f_indices), gl.GL_UNSIGNED_INT, None)


def share_stl_with_gpu(program, vertices, f_indices):
    """
    This part create a shared area in RAM to transfer data to GPU.
    return the shared space as a python dict, allowing to change shared data at realtime.
    The dict is separated in two : uniform for data that does not change from one shader invocation to the other (ie that will be constant over all vertices), and buffer for data that change from one shader invocation to the other, ie vertices, normals color of vertices, etc.
    """
    gpu = { "buffer" : {}, "uniform" : {} }

    gpu["buffer"]["vertices"] = gl.glGenBuffers(1)
    gl.glBindBuffer(gl.GL_ARRAY_BUFFER, gpu["buffer"]["vertices"])
    gl.glBufferData(gl.GL_ARRAY_BUFFER, vertices.nbytes, vertices, gl.GL_DYNAMIC_DRAW)
    stride = vertices.strides[0]

    offset = ctypes.c_void_p(0)
    loc = gl.glGetAttribLocation(program, "a_position")
    gl.glEnableVertexAttribArray(loc)
    gl.glVertexAttribPointer(loc, 3, gl.GL_FLOAT, False, stride, offset)

    offset = ctypes.c_void_p(vertices.dtype["a_position"].itemsize)
    loc = gl.glGetAttribLocation(program, "a_color")
    gl.glEnableVertexAttribArray(loc)
    gl.glVertexAttribPointer(loc, 4, gl.GL_FLOAT, False, stride, offset)

    offset = ctypes.c_void_p(vertices.dtype["a_position"].itemsize + vertices.dtype["a_color"].itemsize)
    loc = gl.glGetAttribLocation(program, "a_normal")
    gl.glEnableVertexAttribArray(loc)
    gl.glVertexAttribPointer(loc, 3, gl.GL_FLOAT, False, stride, offset)

    gpu["buffer"]["filled"] = gl.glGenBuffers(1)
    gl.glBindBuffer(gl.GL_ELEMENT_ARRAY_BUFFER, gpu["buffer"]["filled"])
    gl.glBufferData(gl.GL_ELEMENT_ARRAY_BUFFER, f_indices.nbytes, f_indices, gl.GL_STATIC_DRAW)

    # Bind uniforms (uniform along vertices)
    # --------------------------------------
    gpu["uniform"]["u_model"] = gl.glGetUniformLocation(program, "u_model")
    gl.glUniformMatrix4fv(gpu["uniform"]["u_model"], 1, False, np.eye(4))

    gpu["uniform"]["u_view"] = gl.glGetUniformLocation(program, "u_view")
    view = opengl_matrix_helper.translate(np.eye(4), 0, 0, -200)
    gl.glUniformMatrix4fv(gpu["uniform"]["u_view"], 1, False, view)

    gpu["uniform"]["u_projection"] = gl.glGetUniformLocation(program, "u_projection")
    gl.glUniformMatrix4fv(gpu["uniform"]["u_projection"], 1, False, np.eye(4))

    gpu["uniform"]["u_color"] = gl.glGetUniformLocation(program, "u_color")
    gl.glUniform4f(gpu["uniform"]["u_color"], 1, 1, 1, 1)

    return gpu