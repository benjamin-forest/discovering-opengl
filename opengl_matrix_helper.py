"""
Extracted from book : https://www.labri.fr/perso/nrougier/python-opengl/#modern-opengl

Provide some usefull function to create / manipulate numpy matrices usefull in opengl.
Notably to create Cameras, projection matrices and so on.

"""

import numpy as np
import math

def rotate(M, angle, x, y, z):
    """
    Rotate matrix M of angle angle around vector x, y, z
    """
    angle = math.pi * angle / 180
    c, s = math.cos(angle), math.sin(angle)
    n = math.sqrt(x * x + y * y + z * z)
    x, y, z = x/n, y/n, z/n
    cx, cy, cz = (1 - c) * x, (1 - c) * y, (1 - c) * z
    R = np.array([[cx * x + c, cy * x - z * s, cz * x + y * s, 0],
                  [cx * y + z * s, cy * y + c, cz * y - x * s, 0],
                  [cx * z - y * s, cy * z + x * s, cz * z + c, 0],
                  [0, 0, 0, 1]], dtype=M.dtype).T
    M[...] = np.dot(M, R)
    return M

def translate(M, x, y=None, z=None):
    """
    Translates M matrix. Note the transposition at the end.
    This is mandatory but I do not explain it at the moment. I suppose it is linked to how the matrices are passed to opengl.
    """
    y = x if y is None else y
    z = x if z is None else z
    T = np.array([[1.0, 0.0, 0.0, x],
                  [0.0, 1.0, 0.0, y],
                  [0.0, 0.0, 1.0, z],
                  [0.0, 0.0, 0.0, 1.0]], dtype=M.dtype).T
    M[...] = np.dot(M, T)
    return M

def frustum(left, right, bottom, top, znear, zfar):
    """
    Wikipedia : In 3D computer graphics, the view frustum[1] (also called viewing frustum[2]) is the region of space in the modeled world that may appear on the screen; it is the field of view of a perspective virtual camera system.[1]
    """
    M = np.zeros((4, 4), dtype=np.float32)
    M[0, 0] = +2.0 * znear / (right - left)
    M[2, 0] = (right + left) / (right - left)
    M[1, 1] = +2.0 * znear / (top - bottom)
    M[3, 1] = (top + bottom) / (top - bottom)
    M[2, 2] = -(zfar + znear) / (zfar - znear)
    M[3, 2] = -2.0 * znear * zfar / (zfar - znear)
    M[2, 3] = -1.0
    return M

def perspective(fovy, aspect, znear, zfar):
    """
    From http://learnwebgl.brown37.net/08_projections/projections_perspective.html :

    The fovy parameter stands for the “field of view y-axis” and is the vertical angle of the camera's lens. Common values for fovy range from 30 to 60 degrees. The aspect ratio parameter is the width divided by the height of the canvas window.

    Create a perspective projection matrix using a field-of-view and an aspect ratio.
    @param fovy   Number The angle between the upper and lower sides of the viewing frustum.
    @param aspect Number The aspect ratio of the viewing window. (width/height).
    @param near   Number Distance to the near clipping plane along the -Z axis.
    @param far    Number Distance to the far clipping plane along the -Z axis.
    @return Float32Array The perspective transformation matrix.

    See the website for more information.
    """
    h = math.tan(fovy / 360.0 * math.pi) * znear
    w = h * aspect
    return frustum(-w, w, -h, h, znear, zfar)