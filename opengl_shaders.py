"""
A module to store this project shaders, and the related opengl initialisation of a program.
Only here to improve main file readability
"""

from OpenGL import GL as gl

VERTEX_CODE = """
uniform vec4   u_color;  // Ambiant color
uniform mat4   u_model;         // Model matrix
uniform mat4   u_view;          // View matrix
uniform mat4   u_projection;    // Projection matrix
attribute vec4 a_color;         // Vertex color
attribute vec3 a_position;      // Vertex position
attribute vec3 a_normal;        // Vertex normal
varying vec4   v_color;         // Interpolated fragment color (out)
void main()
{
    gl_Position = u_projection * u_view * u_model * vec4(a_position,1.0);
    vec3 normal = vec3(u_model * vec4(a_normal,0.0));
    vec3 l = vec3(0.0,0.0,1.0);
    // Cosine of the angle between the normal and the light direction,
    // clamped above 0
    //  - light is at the vertical of the triangle -> 1
    //  - light is perpendicular to the triangle -> 0
    //  - light is behind the triangle -> 0
    float cosTheta = clamp( dot( normal, l ), 0.0, 1.0 );
    v_color = u_color * a_color * cosTheta;
}
"""
"""Vertex opengl shader code."""

FRAGMENT_CODE = """
varying vec4 v_color;    // Interpolated fragment color (in)
void main()
{
    gl_FragColor =  v_color;
}
"""
"""Fragment opengl code"""

def init_program():
    """
    Create an opengl program, and associate shaders to it. Compile them in the process.
    @return the opengl program initialized with our shaders.
    """

    program  = gl.glCreateProgram()
    vertex   = gl.glCreateShader(gl.GL_VERTEX_SHADER)
    fragment = gl.glCreateShader(gl.GL_FRAGMENT_SHADER)

    # Set shaders source
    gl.glShaderSource(vertex, VERTEX_CODE)
    gl.glShaderSource(fragment, FRAGMENT_CODE)

    # Compile shaders
    gl.glCompileShader(vertex)
    if not gl.glGetShaderiv(vertex, gl.GL_COMPILE_STATUS):
        error = gl.glGetShaderInfoLog(vertex).decode()
        print(error)
        raise RuntimeError("Vertex shader compilation error")

    gl.glCompileShader(fragment)
    if not gl.glGetShaderiv(fragment, gl.GL_COMPILE_STATUS):
        error = gl.glGetShaderInfoLog(fragment).decode()
        print(error)
        raise RuntimeError("Fragment shader compilation error")
    gl.glAttachShader(program, vertex)
    gl.glAttachShader(program, fragment)
    gl.glLinkProgram(program)

    if not gl.glGetProgramiv(program, gl.GL_LINK_STATUS):
        print(gl.glGetProgramInfoLog(program))
        raise RuntimeError('Linking error')

    gl.glDetachShader(program, vertex)
    gl.glDetachShader(program, fragment)
    gl.glUseProgram(program)
    return program